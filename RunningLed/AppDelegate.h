//
//  AppDelegate.h
//  RunningLed
//
//  Created by Duong Tien Quan on 12/7/15.
//  Copyright © 2015 vtvcab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

